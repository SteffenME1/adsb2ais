class FlightInfo:

    def aircraft_is_valid(self, aircraft):
        self.valid=True
        if "hex" not in aircraft:
            self.valid = False
            return self.valid
        
        if "lat" not in aircraft:
            self.valid = False
            return self.valid
        elif not isinstance(aircraft["lat"],(float,int)):
            self.valid = False
            return self.valid
        elif aircraft["lat"]=="-2147483648&#xb0 -214748.-3648' N":
            self.valid = False
            return self.valid
        
        if "lon" not in aircraft:
            self.valid = False
            return self.valid
        elif not isinstance(aircraft["lon"],(float,int)):
            self.valid = False
            return self.valid
        elif aircraft["lon"]=="-2147483648&#xb0 -214748.-3648' N":
            self.valid = False
            return self.valid

        if "alt_baro" in aircraft:
            if isinstance(aircraft["alt_baro"],(float, int)):
                if aircraft["alt_baro"] > self.config["altitudeThreshold"]:
                    self.valid = False
                    return self.valid

        if "flight" in aircraft:
            for prefix in self.config["ignoredFlightNumberPrefixes"]:
                if aircraft["flight"].startswith(prefix): 
                    self.valid = False
                    return self.valid
        
        return self.valid

    def __init__(self, aircraft, now, config):
        self.config=config
        if self.aircraft_is_valid(aircraft):
            self.second=aircraft["seen_pos"]+now
            self.hex=aircraft["hex"]
            self.lat=float(aircraft["lat"])
            self.lon=float(aircraft["lon"])

            #known name
            if self.hex in config["knownAssets"]:
                self.flight = config["knownAssets"][self.hex] + ": "
            else:
                self.flight=""

            #type 5
            if "flight" in aircraft:
                self.flight = self.flight+aircraft["flight"]
                self.type5=True
            else:
                self.type5=False
            
            #altitude
            try:
                self.alt=int(aircraft["alt_baro"])
                if self.alt<0:
                    self.alt=0
            except:
                self.alt=0
            
            #course over ground
            try:
                self.cog=float(aircraft["true_heading"])
            except:
                self.cog=0

            #speed over ground
            try:
                self.sog=float(aircraft["gs"])
            except:
                self.sog=0
        else: 
            return None