import pyais
import socket
import json
from FlightInfo import FlightInfo
import time
import os

class Adsb2Ais():

    def __init__(self, config):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        self.UDP_IP = config["outIP"]
        self.UDP_PORT = config["outPort"]
        self.src=config["aircraftFile"]
        self.use_tcp_for_aircraft_file = config.get("useTCPForAircraftFile", False)
        self.aircraftFileHostIP = config.get("aircraftFileHostIP", "")
        self.aircraftFileHostPort = int(config.get("aircraftFileHostPort", "0"))
        self.config=config
        self.names={}

    def send_NMEA(self, data):
        msg = pyais.encode.encode_dict(data, radio_channel="A", talker_id="AIVDM")
        for pack in msg:
            arr2 = bytes(str(pack),'ascii')
            self.sock.sendto(arr2, (self.UDP_IP, self.UDP_PORT))

    def get_data_type_9(self, flightInfo):
        data = {
            'repeat':3,
            'mmsi': str(self.names[flightInfo.hex]),
            'course': flightInfo.cog,
            'lat': flightInfo.lat,
            'lon': flightInfo.lon,
            'alt':flightInfo.alt,
            'speed':flightInfo.sog,
            'type':9
        }
        return data
    
    def get_data_type_5(self,flightInfo):
        data = {
            'repeat':3,
            'mmsi': str(self.names[flightInfo.hex]),
            'shipname':flightInfo.flight,
            'course': flightInfo.cog,
            'lat': flightInfo.lat,
            'lon': flightInfo.lon,
            'alt':flightInfo.alt,
            'speed':flightInfo.sog,
            'type':5
        }
        return data

    def read_aircrafts_from_port(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            s.connect((self.aircraftFileHostIP, self.aircraftFileHostPort))
            data = s.recv(1024)
            aircrafts_data = data.decode('utf-8')

        except socket.error as e:
            print(e)
            return

        finally:
            s.close()
        return json.loads(aircrafts_data)

    def get_aircrafts(self):
        if self.use_tcp_for_aircraft_file:
            aircrafts = self.read_aircrafts_from_port()
        else:
            try:
                with open(self.src, 'r') as src_file:
                    aircrafts = json.load(src_file)
            except IOError:
                print("Could not find aircrafts.json. Retrying...")
                return
        return aircrafts

    def loop(self):
        mmsi=999999999
        count=1

        while 1:
            count+=1
            aircrafts = self.get_aircrafts()
            if not aircrafts:
                time.sleep(2)
                continue
            now=aircrafts["now"]

            for aircraft in aircrafts["aircraft"]:
                flightInfo = FlightInfo(aircraft, now, self.config)
                if flightInfo.valid:
                    if flightInfo.hex not in self.names:
                        mmsi -= 1
                        self.names[flightInfo.hex]=mmsi
                    if count % 3 == 0 and flightInfo.type5:
                        data=self.get_data_type_5(flightInfo)
                        self.send_NMEA(data)
                    data=self.get_data_type_9(flightInfo)
                    self.send_NMEA(data)
            if count % 3 == 0:
                count=0
            time.sleep(2)

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c","--config_file",
                        type=str,
                        default="config.json",
                        help="path to config file")
    args=parser.parse_args()
    with open(args.config_file,'r') as src:
        config=json.load(src)
    adsb2ais=Adsb2Ais(config)
    adsb2ais.loop()